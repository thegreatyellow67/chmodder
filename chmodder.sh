#!/bin/bash

# #############################################################################
#
#       _                         _     _                     _
#   ___| |__  _ __ ___   ___   __| | __| | ___ _ __       ___| |__
#  / __| '_ \| '_ ` _ \ / _ \ / _` |/ _` |/ _ \ '__|     / __| '_ \
# | (__| | | | | | | | | (_) | (_| | (_| |  __/ |     _  \__ \ | | |
#  \___|_| |_|_| |_| |_|\___/ \__,_|\__,_|\___|_|    (_) |___/_| |_|
#
# chmodder.sh
#
# author: TheGreatYellow67
# https://gitlab.com/thegreatyellow67
#
# creation date: 2021/04/23
# modified date: 2021/04/29
#
# Generic Script for recursively setting permissions for directories and files
# to defined or default permissions using chmod.
#
# Takes a path to recurse through and options for specifying directory and/or
# file permissions.
# Outputs a list of affected directories and files.
#
# The script needs a valid ROOT path, otherwise it will return an usage message.
# permissions to the default for most OSs (dirs: 755, files: 644).
# #############################################################################

# ANSI color codes
#
# Black        0;30     Dark Gray     1;30
# Red          0;31     Light Red     1;31
# Green        0;32     Light Green   1;32
# Brown/Orange 0;33     Yellow        1;33
# Blue         0;34     Light Blue    1;34
# Purple       0;35     Light Purple  1;35
# Cyan         0;36     Light Cyan    1;36
# Light Gray   0;37     White         1;37
#
# Constants
#
LCyan="\033[1;36m"	# Light Cyan
LGreen="\033[1;32m" # Light Green
Cyan="\033[0;36m"	# Cyan
Red='\033[0;31m'	# Red
LRed='\033[1;31m'	# Light Red
Yellow='\033[1;33m'	# Yellow
Nc='\033[0m' 		# No Color

clear

# Help message
help()
{
  printf "${LCyan}"
  echo
  echo '  ----------------------------------------------------------------'
  echo ' |  [ chmodder.sh ]                                               |'
  echo ' |  by TheGreatYellow67                                           |'
  echo ' |  https://gitlab.com/thegreatyellow67                           |'
  echo ' |                                                                |'
  echo ' |  Generic Script for recursively setting permissions for        |'
  echo ' |  directories and files to defined or default permissions       |'
  echo ' |  using chmod.                                                  |'
  echo ' |                                                                |'
  echo ' |  Usage: -p ROOT -d DIRPERMS -f FILEPERMS                       |'
  echo ' |                                                                |'
  echo ' |  -p ROOT, directory path you wish to modify permissions for    |'
  echo ' |  -d DIRPERMS, directory permissions (default 755)              |'
  echo ' |  -f FILEPERMS, file permissions (default 644)                  |'
  echo ' |                                                                |'
  echo ' |  CAREFUL! CAREFUL! CAREFUL!                                    |'
  echo ' |  Use this script with caution, change dir and file             |'
  echo ' |  permissions only in paths where you are absolutely sure       |'
  echo ' |  of what you are doing! Check twice the given path!            |'
  echo ' |  This script could damage your Linux system!                   |'
  echo ' |                                                                |'
  echo " |  Don't say I didn't warn you, guys!!                           |"
  echo ' |  ------------------------------------------------------------  |'
  echo ' |  Example1:                                                     |'
  echo ' |  changes permissions with all given options                    |'
  echo ' |  ./chmodder.sh -p /path/of/root_dir -d 744 -f 600              |'
  echo ' |                                                                |'
  echo ' |  Example2:                                                     |'
  echo ' |  changes permissions using -d and -f default values            |'
  echo ' |  ./chmodder.sh -p /path/of/root_dir                            |'
  echo ' |                                                                |'
  echo ' |  Example3:                                                     |'
  echo ' |  changes permissions using -d (700) and -f as default (644)    |'
  echo ' |  ./chmodder.sh -p /path/of/root_dir -d 700                     |'
  echo ' |                                                                |'
  echo ' |  Example4:                                                     |'
  echo ' |  changes permissions using -f (777) and -d as default (755)    |'
  echo ' |  ./chmodder.sh -p /path/of/root_dir -f 777                     |'
  echo '  ----------------------------------------------------------------'
  echo
  printf "${Nc}"
  exit 1
}

# Usage message
usage()
{
  printf "${LRed}"
  echo
  echo '  ----------------------------------------------------------------'
  echo ' |  [ chmodder.sh ]                                               |'
  echo ' |  by TheGreatYellow67                                           |'
  echo ' |  https://gitlab.com/thegreatyellow67                           |'
  echo ' |                                                                |'
  echo ' |  Generic Script for recursively setting permissions for        |'
  echo ' |  directories and files to defined or default permissions       |'
  echo ' |  using chmod.                                                  |'
  echo ' |                                                                |'
  echo ' |  Usage: -p ROOT -d DIRPERMS -f FILEPERMS                       |'
  echo ' |                                                                |'
  echo ' |  -p ROOT, directory path you wish to modify permissions for    |'
  echo ' |  -d DIRPERMS, directory permissions (default 755)              |'
  echo ' |  -f FILEPERMS, file permissions (default 644)                  |'
  echo ' |  ------------------------------------------------------------  |'
  echo ' |                                                                |'
  echo ' |  Also see help for examples (./chmodder.sh -h)                 |'
  echo ' |                                                                |'
  echo '  ----------------------------------------------------------------'
  echo
  printf "${Nc}"
  exit 1
}

# Check if user entered arguments
if [ $# -lt 1 ] ; then
 usage
fi

if [ $# -eq 1 ] && [ "$1" = "-h" ]; then
 help
fi

# Get options
while getopts "p:d:f:" opt; do
  case "$opt" in
    p) ROOT="$OPTARG";;
    d) DIRPERMS="$OPTARG";;
    f) FILEPERMS="$OPTARG";;
	\?) usage;;
  esac
done
shift $((OPTIND -1))

# Check if user entered a path and if it's valid
if [ -z "$ROOT" ]; then
  printf "${LRed}"
  echo
  echo " ROOT argument not passed to this script!"
  echo " Please re-run script with a valid path."
  echo
  printf "${Nc}"
  exit 1
elif [ ! -d "$ROOT" ] ; then
  printf "${LRed}"
  echo
  echo " Directory: $ROOT"
  echo " does not exist or isn't a directory!"
  echo
  printf "${Nc}"
 exit 1
fi

# Default directory permissions, if not set on command line
if [ -z "$DIRPERMS" ] ; then
  DIRPERMS=755
fi

# Default file permissions, if not set on command line
if [ -z "$FILEPERMS" ] ; then
  FILEPERMS=644
fi

printf "${LGreen}"
echo
echo "   -----------------------------------------------------------"
echo "  |  [ chmodder.sh ]                                          |"
echo "  |  by TheGreatYellow67                                      |"
echo "  |  https://gitlab.com/thegreatyellow67                      |"
echo "  |                                                           |"
echo "  |  Generic Script for recursively setting permissions for   |"
echo "  |  directories and files to defined or default permissions  |"
echo "  |  using chmod.                                             |"
echo "   -----------------------------------------------------------"
echo
echo "  Values for changing permissions:"
echo
echo "  Folder path: $ROOT"
echo "  Dir permissions: $DIRPERMS"
echo "  File permissions: $FILEPERMS"
echo
printf "${Nc}"
read -rsn1 -p "  Press any key to continue, ctrl+c to cancel script...";echo

clear;echo
# Recursively set directory/file permissions based on the permission variables
if [ -n "$DIRPERMS" ] ; then
  find $ROOT -type d -print0 | xargs -0 chmod -v $DIRPERMS
fi

if [ -n "$FILEPERMS" ] ; then
  find $ROOT -type f -print0 | xargs -0 chmod -v $FILEPERMS
fi
echo

# chmodder.sh

**by TheGreatYellow67**
**( https://gitlab.com/thegreatyellow67 )**

Generic Script for recursively setting permissions for
directories and files to defined or default permissions
using chmod.

`
Usage: -p ROOT -d DIRPERMS -f FILEPERMS
`

**-p ROOT**, directory path you wish to modify permissions for

**-d DIRPERMS**, directory permissions (default 755)

**-f FILEPERMS**, file permissions (default 644)

## **CAREFUL! CAREFUL! CAREFUL!**

Use this script with caution, change dir and file permissions only in paths where you are absolutely sure of what you are doing!

Check twice the given path!

This script could damage your Linux system! **Don't say I didn't warn you, guys!!**

---

**Example1:**

changes permissions with all given options

```
./chmodder.sh -p /path/of/root_dir -d 744 -f 600
```

---

**Example2:**

changes permissions using -d and -f default values

```
./chmodder.sh -p /path/of/root_dir
```

---

**Example3:**

changes permissions using -d (700) and -f as default (644)

```
./chmodder.sh -p /path/of/root_dir -d 700
```

---

**Example4:**

changes permissions using -f (777) and -d as default (755)

```
./chmodder.sh -p /path/of/root_dir -f 777
```
